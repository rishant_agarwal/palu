import moment from "moment"
export const DATE_FORMAT = "YYYYMMDD"
export const DATE_TODAY = moment().format(DATE_FORMAT)
