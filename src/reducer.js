import { createStore, combineReducers } from "redux"
import { ADD_CAMPAIGN, EDIT_CAMPAIGN, GET_INITIAL_DATA } from "./constants.js"
import { composeWithDevTools } from "redux-devtools-extension"
import {
	createNewCampaignObject,
	returnProcessedInitialData
} from "./utils/utils.js"

let initialState = {
	data: []
}

let getNewId = data => data.length
let getEditedData = (data, id, newData) => {
	let toBeEditedObj = data.filter(record => record.id == id),
		dataWithoutEditedObj = data.filter(record => record.id != id)

	return dataWithoutEditedObj.concat({ ...toBeEditedObj, ...newData })
}

let appReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_CAMPAIGN:
			return {
				...state,
				data: state.data.concat({
					...createNewCampaignObject(action.data),
					id: getNewId(state.data)
				})
			}
		case EDIT_CAMPAIGN:
			return {
				...state,
				data: getEditedData(state.data, action.data.id, action.data)
			}
		case GET_INITIAL_DATA:
			return {
				...state,
				data: state.data.concat(returnProcessedInitialData())
			}
		default:
			return state
	}
}

let store = createStore(combineReducers({ appReducer }), initialState)

export default store
