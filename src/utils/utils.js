import moment from "moment"
import { extendMoment } from "moment-range"

import { DATE_TODAY, DATE_FORMAT } from "./../config.js"

const extendedMoment = extendMoment(moment)

export let getInitialDataTmp = () => {
	let data = [
		{
			id: 0,
			name: "Renix | Summersale | MobPro",
			budget: 40000.0,
			startDate: "2017-07-17T00:00:00.000",
			endDate: "2017-08-27T23:59:59.999",
			stats: [
				{
					timestamp: "2017-07-17T00:00:00.000",
					spend: 410.5,
					views: 49436,
					clicks: 208
				},
				{
					timestamp: "2017-07-18T00:00:00.000",
					spend: 589.79,
					views: 40896,
					clicks: 228
				},
				{
					timestamp: "2017-07-19T00:00:00.000",
					spend: 468.64,
					views: 56123,
					clicks: 256
				},
				{
					timestamp: "2017-07-20T00:00:00.000",
					spend: 542.19,
					views: 51660,
					clicks: 218
				},
				{
					timestamp: "2017-07-21T00:00:00.000",
					spend: 412.78,
					views: 44183,
					clicks: 204
				},
				{
					timestamp: "2017-07-22T00:00:00.000",
					spend: 530.63,
					views: 43866,
					clicks: 282
				},
				{
					timestamp: "2017-07-23T00:00:00.000",
					spend: 429.49,
					views: 58168,
					clicks: 210
				},
				{
					timestamp: "2017-07-24T00:00:00.000",
					spend: 507.11,
					views: 43344,
					clicks: 247
				},
				{
					timestamp: "2017-07-25T00:00:00.000",
					spend: 545.62,
					views: 48389,
					clicks: 288
				},
				{
					timestamp: "2017-07-26T00:00:00.000",
					spend: 438.28,
					views: 55135,
					clicks: 227
				},
				{
					timestamp: "2017-07-27T00:00:00.000",
					spend: 477.48,
					views: 52358,
					clicks: 253
				}
			]
		},
		{
			id: 1,
			name: "Zmusic | Zomercampagne",
			budget: 30000.0,
			startDate: "2017-07-22T00:00:01.000",
			endDate: "2017-08-22T23:59:59.999",
			stats: [
				{
					timestamp: "2017-07-22T00:00:00.000",
					spend: 1145.52,
					views: 81451,
					clicks: 409
				},
				{
					timestamp: "2017-07-23T00:00:00.000",
					spend: 1154.99,
					views: 119423,
					clicks: 423
				},
				{
					timestamp: "2017-07-24T00:00:00.000",
					spend: 907.44,
					views: 104131,
					clicks: 575
				},
				{
					timestamp: "2017-07-25T00:00:00.000",
					spend: 1151.3,
					views: 93878,
					clicks: 427
				},
				{
					timestamp: "2017-07-26T00:00:00.000",
					spend: 1034.63,
					views: 116606,
					clicks: 599
				},
				{
					timestamp: "2017-07-27T00:00:00.000",
					spend: 931.77,
					views: 112504,
					clicks: 554
				}
			]
		},
		{
			id: 2,
			name: "Jimmy's Hotdog Q3",
			budget: 10000.0,
			startDate: "2017-08-01T08:00:00.000",
			endDate: "2017-09-01T23:59:00.000",
			stats: []
		}
	]

	return data
}

export let returnProcessedInitialData = () => {
	return getInitialDataTmp().map(record => createNewCampaignObject(record))
}

export let createNewCampaignObject = params => {
	let newCampaignInitialObject = {
		id: 0,
		name: "",
		budget: 0,
		startDate: DATE_TODAY,
		endDate: DATE_TODAY,
		stats: [],
		totalViews: 0,
		totalClicks: 0,
		totalSpend: 0,
		isBudgetExceeded: false
	}
	return { ...newCampaignInitialObject, ...params }
}

export let isDateBetweenGivenDates = params => {
	let { date, startDate, endDate } = params
	let range = moment().range(
		moment(startDate).format(DATE_FORMAT),
		moment(endDate).format(DATE_FORMAT)
	)

	return range.contains(date)
}

export let getDataBetweenDatesAllInclusive = params => {
	let { data, startDate, endDate, dateParam } = params
	return data.filter(record =>
		isDateBetweenGivenDates(record[dateParam], startDate, endDate)
	)
}

export let getSumForProp = params => {
	let { data, prop } = params
	data.map(record => record[prop]).reduce((initial, current) => {
		return initial + current
	}, 0)
}
