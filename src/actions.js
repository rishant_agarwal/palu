import { GET_INITIAL_DATA, ADD_CAMPAIGN, EDIT_CAMPAIGN } from "./constants.js"

export const getInitialData = data => {
	return { type: GET_INITIAL_DATA, data }
}

export const addCampaign = data => {
	return { type: ADD_CAMPAIGN, data }
}

export const editCampaign = data => {
	return { type: EDIT_CAMPAIGN, data }
}
