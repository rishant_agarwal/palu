import React, { Component } from "react"
import { connect } from "react-redux"
import moment from "moment"

import DataTable from "./components/DataTable"
import Graph from "./components/Graph"
import Modal from "./components/Modal"

import { getInitialData, addCampaign, editCampaign } from "./actions.js"

import { DATE_FORMAT } from "./config.js"

import "./App.css"

class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
			showModal: false,
			data: {
				id: null,
				name: "",
				startDate: moment().format("YYYY-MM-DD"),
				endDate: moment().format("YYYY-MM-DD"),
				budget: 0
			}
		}

		this.addCampaign = this.addCampaign.bind(this)
		this.editCampaign = this.editCampaign.bind(this)
		this.closeModal = this.closeModal.bind(this)
		this.openModal = this.openModal.bind(this)
		this.handleFormChange = this.handleFormChange.bind(this)
		this.clearForm = this.clearForm.bind(this)
		this.getRecordById = this.getRecordById.bind(this)
		this.setModalData = this.setModalData.bind(this)
		this.saveEditedData = this.saveEditedData.bind(this)

		this.edit = false
		// this.getBudget = this.getBudget.bind(this)
	}

	setModalData(data) {
		this.setState({ data })
	}

	closeModal() {
		this.setState({ showModal: false })
	}

	openModal() {
		this.setState({ showModal: true })
	}

	addCampaign(data) {
		this.props.addCampaign(data)
	}

	getRecordById(id) {
		return this.props.data.filter(record => record.id == id)[0]
	}

	saveEditedData(data) {
		this.props.editCampaign(data)
	}

	editCampaign(campaignId) {
		this.edit = true
		this.setModalData(this.getRecordById(campaignId))
		this.openModal()
	}

	componentDidMount() {
		this.props.getInitialData()
	}

	handleFormChange(event) {
		let { name, value } = event.target
		let tmpObject = {}
		tmpObject[name] = value
		this.setState({ data: { ...this.state.data, ...tmpObject } })
	}

	clearForm() {
		this.setState({
			data: {
				id: null,
				name: "",
				startDate: moment().format("YYYY-MM-DD"),
				endDate: moment().format("YYYY-MM-DD"),
				budget: 0
			}
		})
	}

	render() {
		let data = this.props.data.map(record => ({
			...record,
			totalSpend: record.stats.reduce(function(initial, current) {
				return initial + current.spend
			}, 0),
			totalViews: record.stats.reduce(function(initial, current) {
				return initial + current.views
			}, 0),
			totalClicks: record.stats.reduce(function(initial, current) {
				return initial + current.clicks
			}, 0)
		}))

		return (
			<div className="App container main_panel">
				<h3 className="title-content-header header">
					<span>MobPro DSP-Home</span>
				</h3>
				<Graph data={this.props.data} />
				<Modal
					createNewCampaign={this.addCampaign}
					openModal={this.openModal}
					closeModal={this.closeModal}
					showModal={this.state.showModal}
					data={this.state.data}
					handleFormChange={this.handleFormChange}
					clearForm={this.clearForm}
					saveEditedData={this.saveEditedData}
				/>
				<DataTable data={data} editCampaign={this.editCampaign} />
			</div>
		)
	}
}

let mapDispatchToProps = dispatch => {
	return {
		addCampaign: data => dispatch(addCampaign(data)),
		editCampaign: data => dispatch(editCampaign(data)),
		getInitialData: () => dispatch(getInitialData())
	}
}

let mapStateToProps = state => {
	return {
		data: state.appReducer.data
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(App)
