import React, { Component } from "react"
import { Modal, Button } from "react-bootstrap"
import moment from "moment"

class Popup extends Component {
	constructor(props) {
		super(props)

		this.save = this.save.bind(this)
		this.close = this.close.bind(this)
	}

	close() {
		this.props.closeModal()
		this.props.clearForm()
	}

	save() {
		this.props.data.id == null
			? this.props.createNewCampaign(this.props.data)
			: this.props.saveEditedData(this.props.data)
		this.props.closeModal()
		this.props.clearForm()
	}

	render() {
		let { startDate, endDate, budget, name, id } = this.props.data
		return (
			<div>
				<button
					type="btn"
					className="btn btn-success"
					id="add"
					onClick={this.props.openModal}
				>
					Add Campaign
				</button>
				<Modal show={this.props.showModal} onHide={this.close}>
					<Modal.Header closeButton>
						<Modal.Title>MobPro DSP - Campaign</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<form>
							<ul className="flex-outer">
								<li>
									<label>Name</label>
									<input
										type="text"
										id="first-name"
										name="name"
										placeholder="Enter your name"
										value={name}
										onChange={this.props.handleFormChange}
									/>
								</li>
								<li>
									<label>Start date</label>
									<div id="startDate" className="input-append date">
										<input
											data-format="dd/MM/yyyy"
											type="date"
											name="startDate"
											value={moment(startDate).format("YYYY-MM-DD")}
											onChange={this.props.handleFormChange}
										/>{" "}
										<span className="add-on">
											<i
												onClick={this.handleClickDate}
												data-time-icon="icon-time"
												data-date-icon="icon-calendar"
												className="icon-calendar"
											/>
										</span>
									</div>
								</li>
								<li>
									<label>End date</label>
									<div id="endDate" className="input-append date">
										<input
											data-format="dd/MM/yyyy"
											type="date"
											value={moment(endDate).format("YYYY-MM-DD")}
											name="endDate"
											onChange={this.props.handleFormChange}
										/>{" "}
										<span className="add-on">
											<i
												data-time-icon="icon-time"
												data-date-icon="icon-calendar"
												className="icon-calendar"
											/>
										</span>
									</div>
								</li>
								<li>
									<label> Budget</label>
									<input
										name="budget"
										type="number"
										value={budget}
										onChange={this.props.handleFormChange}
									/>
								</li>
							</ul>
						</form>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={this.close}>Cancel</Button>
						<Button onClick={this.save}>Save</Button>
					</Modal.Footer>
				</Modal>
			</div>
		)
	}
}

export default Popup
