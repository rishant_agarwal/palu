import React, { Component } from "react"
import LineChart from "react-linechart"
import "./../../node_modules/react-linechart/dist/styles.css"
import moment from "moment"
import { DATE_FORMAT } from "./../config.js"

let REFERENCE = moment("20170727") // fixed just for testing, use moment();
let A_WEEK_OLD = REFERENCE.clone().subtract(7, "days").startOf("day")

function isWithinAWeek(momentDate) {
	return momentDate.isAfter(A_WEEK_OLD)
}

export default class Graph extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		let dateBucket = {}
		dateBucket[REFERENCE.clone().format(DATE_FORMAT)] = 0
		dateBucket[REFERENCE.clone().subtract(1, "day").format(DATE_FORMAT)] = 0
		dateBucket[REFERENCE.clone().subtract(2, "day").format(DATE_FORMAT)] = 0
		dateBucket[REFERENCE.clone().subtract(3, "day").format(DATE_FORMAT)] = 0
		dateBucket[REFERENCE.clone().subtract(4, "day").format(DATE_FORMAT)] = 0
		dateBucket[REFERENCE.clone().subtract(5, "day").format(DATE_FORMAT)] = 0
		dateBucket[REFERENCE.clone().subtract(6, "day").format(DATE_FORMAT)] = 0

		let graphData = this.props.data
			.reduce(function(initial, current) {
				return initial.concat(current.stats)
			}, [])
			.map(record => ({
				...record,
				timestamp: moment(record.timestamp).format(DATE_FORMAT)
			}))
			.filter(record => isWithinAWeek(moment(record.timestamp)))
			.map(record => {
				dateBucket[record.timestamp] += +record.clicks
			})

		let graphDataPoints = []
		for (var rec in dateBucket) {
			graphDataPoints.push({ x: rec, y: dateBucket[rec] })
		}
		// console.log(graphDataPoints)

		const data = [
			{
				color: "steelblue",
				points: graphDataPoints
			}
		]
		return (
			<div>
				<div className="graph">
					<LineChart width={600} height={300} data={data} xLabel = "Last seven days" yLabel = "Spend Last Week"/>
				</div>
			</div>
		)
	}
}
