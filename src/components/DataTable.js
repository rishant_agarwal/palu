import React, { Component } from "react"
import { Table, Column, Cell } from 'fixed-data-table-2';
import Dimensions from "react-dimensions"
import "./../../node_modules/fixed-data-table/dist/fixed-data-table.css"
import { StyleSheet, css } from 'aphrodite'

class DataTable extends Component {
	constructor(props) {
		super(props)
	}

	render() {
	
	let {data, editCampaign, height, width, containerHeight, containerWidth, ...props} = this.props;
	
	return (
			<Table
				className={css(styles.wrapperStyles)}
				rowHeight={50}
				rowsCount={data.length}
			    width={containerWidth}
	        	height={containerHeight}
				headerHeight={50}
			>
				<Column
					header={<Cell className={css(styles.newTableHeader)}>Name</Cell>}
					cell={({ rowIndex, ...props }) =>
						<Cell {...props}>
							{data[rowIndex]["name"]}
						</Cell>}
					flexGrow={2}
					width={300}
				/>
				<Column
					header={<Cell className={css(styles.newTableHeader)} >Total Spends</Cell>}
					cell={({ rowIndex, ...props }) =>
						<Cell {...props}>
							{data[rowIndex]["totalSpend"]}
						</Cell>}
					width={150}
					flexGrow={1}
				/>
				<Column
					header={<Cell className={css(styles.newTableHeader)}>Total Views</Cell>}
					cell={({ rowIndex, ...props }) =>
						<Cell {...props}>
							{data[rowIndex]["totalViews"]}
						</Cell>}
					width={150}
					flexGrow={1}
				/>
				<Column
					header={<Cell className={css(styles.newTableHeader)}>Total Clicks</Cell>}
					cell={({ rowIndex, ...props }) =>
						<Cell {...props}>
							{data[rowIndex]["totalClicks"]}
						</Cell>}
					width={150}
					flexGrow={2}
				/>
				<Column
					header={<Cell className={css(styles.newTableHeader)}>Action</Cell>}
					cell={({ rowIndex, ...props }) =>
						<Cell {...props}>
							<a href="#" onClick={editCampaign.bind(this, data[rowIndex]["id"])}>
								Edit
							</a>
						</Cell>}
					width={150}
					flexGrow={2}
				/>
			</Table>
		)
	}
}

const styles = StyleSheet.create({
 wrapperStyles: {
    border: 'none',
    height: '100%'
  },
  newTableHeader: {
    color: '#fff',
    fontFamily: 'arial',
    fontSize: '14px',
    lineHeight: '1',
    background: '#78a4c8',
    border: 'none'
  }
});

export default Dimensions({
  getHeight: function(element) {
    return window.innerHeight - 400;
  },
  getWidth: function(element) {
    var widthOffset = window.innerWidth < 600 ? 0 : 300;
    return window.innerWidth - widthOffset;
  }
})(DataTable) 
